Framabot est un petit outils développé en 2013 par JosephK qui permet de suivre
quand sortent les nouvelles versions des logiciels et
quelles versions sont proposées sur la Framakey, Framapack et les Framadvd.

En gros, d'avoir une vue synthétique de tout ce ça.

[bot.framasoft.org](https://bot.framasoft.org)

Le logiciel scanne quotidiennement les sites officiels (flux rss, dépôt git…) des logiciels à
la recherche du numéro de version en fonction de la regex qu'on définie manuellement.
(Il n'y a pas de tâche cron, chaque consultation du site ou du flux RSS, traite un lot de 10 logiciels)

L'utilisateur et le mot de passe par défaut sont `admin` / `admin`, pour les changer, il faut
remplacer `$login_pass` dans le fichier `config.php` par le md5 de `votre_login:votre_mot_de_passe`.

Le logiciel est sous licence MIT.

Il n'est plus maintenu mais mériterait grandement d'être amélioré :)
