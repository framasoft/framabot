$(document).ready(function() {
	$('#bot_atom').attr('href','scan.php?atom=admin');	
/********************************* 
 * Formulaire d'ajout de logiciel
 *********************************/
	$('.footer').before(
'	<div id="add_form"><div class="add_form_container">'+
'		<form id="add_app" action="edit.php" method="post" role="form" class="form-horizontal">'+
'		<div class="form-group" style="width:50%;float:left;padding:0px 20px;">'+
'			<label for="inputName">Nom</label>'+
'			<input class="form-control" required name="name" type="text" id="inputName" placeholder="libreoffice"/>'+
'			<br />'+
'			<label for="inputSite">Site</label><a id="site_link" href="" title="Site Officiel"><img src="images/site.png" /></a>'+
'			<input class="form-control" required name="site" type="text" id="inputSite" placeholder="http://fr.libreoffice.org/"/>'+
'		</div>'+
'		<div style="width:50%;float:left;padding:0px 20px;">'+
'			<label for="inputFramasoft">Framasoft</label><a id="fs_search" href="http://www.google.com/cse?cx=017931506014566040656%3Aiqp-lrqbqlu&q=" title="Chercher sur Framasoft"><img src="images/search.png" /></a>'+
'			<input class="form-control" name="fs" type="text" id="inputFramasoft" placeholder="article5061.html"/>'+
'			<br />'+
'			<label for="inputWikipedia">Wikipédia</label><a id="wp_search" href="http://fr.wikipedia.org/wiki/Special:Recherche?search=" title="Chercher sur Wikipédia"><img src="images/search.png" /></a>'+
'			<input class="form-control" name="wp" type="text" id="inputWikipedia" placeholder="LibreOffice"/>'+
'		</div>'+
'		<div style="clear:left">'+
'			<label for="inputURLtoScan">URL à scanner</label><a id="urltoscan_link" href="" title="Lien Url à scanner"><img src="images/site.png" /></a>'+
'			<input class="form-control" required name="urltoscan" type="text" id="inputURLtoScan" placeholder="http://fr.libreoffice.org/telecharger"/>'+
'			<br />'+
'			<label for="inputRegEx">RegEx de scan</label><img id="regex" src="images/regex.png" title="Modèle : #texte avant([0-9\.]+)texte après# &#10;Ne pas insérer de &lt;tag&gt; HTML ni de &quot;" alt="R" />'+
'			<input class="form-control" required name="pattern" type="text" id="inputRegEx" placeholder="#libreoffice/stable/([0-9\\.]+)/win/x86#"/>'+
'		</div>'+
'		<p style="text-align:center; padding-top:10px;">'+
'			<input type="hidden" name="action" value="add" />'+
'			<input class="btn btn-success" id="add_submit" type="submit" value="Valider" />&nbsp;'+
'			<input class="btn btn-default" id="add_cancel" type="button" value="Annuler" />'+
'		</p>'+
'		</form>'+
'	</div></div>'
	);
	
	$('.table-sort-search-input').before('<a href="javascript:void(0);" title="Ajouter un logiciel" id="add_btn" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-plus"></span></a>');
	$('.table-sort-search-input').after('<a href="javascript:void(0);" title="Se déconnecter" id="logout_btn" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-off"></span></a>');
	$('#login_btn').hide();
	
	// Raccourcis Formulaire ajout/edit
	// Liens search dans le formulaire Frama et Wikipedia
	$('#inputName').focusout(function() {
		$('#fs_search').attr('href','http://www.google.com/cse?cx=017931506014566040656%3Aiqp-lrqbqlu&q='+$('#inputName').val());
		$('#wp_search').attr('href','http://fr.wikipedia.org/wiki/Special:Recherche?search='+$('#inputName').val());
	});
	// Lien site officiel
	$('#inputSite').focusout(function() {$('#site_link').attr('href',$('#inputSite').val());});
	// Lien urltoscan
	$('#inputURLtoScan').focusout(function() {$('#urltoscan_link').attr('href',$('#inputURLtoScan').val());	});
	$('#add_form').hide();
	// Lien remplissage regex
	$('#regex').click(function() {
		var regex=$('#inputRegEx').val();
		$('#inputRegEx').val(regex+'#([0-9\\.]+)#')
	});
	
	$('#add_btn').click(function() {
		// Nettoyage préliminaire
		$('#add_form input[type="text"]').val('');
		if($('#add_form input[name="action"]').val()=='edit') {
			$('#add_form input[name="action"]').val('add');
			$('#inputName').removeAttr('disabled');
		}
		// Affichage du formulaire
		$('#add_form').show();
	});
	var dataString = '';
// Requête AJAX pour ajouter/edit le logiciel
	$('#add_submit').click(function() {
		$('#inputName').removeAttr('disabled');
		dataString = $('#add_form form').serialize();
		var app=$('#inputName').val();
		$.ajax({
			url: "edit.php",
            type: "POST",
            data: dataString,
            dataType: 'json',
            success: function(json) {
			    if(json.message != '') {
					$('#add_app p').after('<div class="alert alert-success">Parfait ! '+json.message+'<img src="scan.php?c='+app+'&f" width="1" height="1"</div>');
					$('#add_app input[type="text"]').val('');
					$('#add_form').delay(4000).fadeOut('slow');

					$('#'+app).removeAttr('id');window.location.hash = "#"+app;
					window.setTimeout(window.location.reload(), 5000); // Flemme de faire un rafraichissment dynamique
				} else {
					$('#add_app p').after('<div class="alert alert-error">Erreur : vérifiez que le dossier cache est accessible en écriture<br />'+json.message+'</div>');
				}
				$('#add_app .alert').delay(3500).fadeOut('slow');
			}
		});
		return false;
	});
// Fermeture du formulaire d'ajout/edition de logiciel	
	$('#add_cancel').click(function() {	$('#add_form').fadeOut('slow');});

/***********************
 *  Déconnecter
 ***********************/
	$('#logout_btn').click(function() {		
		$.ajax({
			url: "index.php",
            type: "POST",
            data: 'logout=true',
            success: function() {
			 	window.setTimeout('location.reload()');
			}
		});
		return false;
	});
});
