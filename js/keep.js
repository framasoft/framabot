/*********************
 *  Formulaire Edit 
 *********************/
	
	$('a.app_edit').click(function() {
		$('#add_form').show();
		var app=$(this).parents('tr').attr('id');
		$.ajax({
			url: "cache/apps/"+app+".txt",
			cache:false,
            dataType: 'json',
            success: function(data) {
				$('#inputSite').val(data.site);
				$('#inputFramasoft').val(data.fs);
				$('#inputWikipedia').val(data.wp);
				$('#inputURLtoScan').val(data.urltoscan);
				$('#inputRegEx').val(data.pattern);
				
				$('#site_link').attr('href',data.site);
				$('#urltoscan_link').attr('href',data.urltoscan);
			},
		});		
		$('#inputName').attr('disabled','')
		$('#inputName').val(app);
		$('#add_form input[name="action"]').val('edit');
		$('#fs_search').attr('href','http://www.google.com/cse?cx=017931506014566040656%3Aiqp-lrqbqlu&q='+app);
		$('#wp_search').attr('href','http://fr.wikipedia.org/wiki/Special:Recherche?search='+app);
	});
	
/***********************
 *  Framakey Urgent
 ***********************/
	$('.fk').click(function() {
		var app=$(this).parents('tr').attr('id');
		$.ajax({
			url: "edit.php",
            type: "POST",
            data: 'name='+app+'&action=fk',
            dataType: 'json',
            success: function(json) {
			    if(json.message != '') {
					$('#'+app+' .fk').attr('src','images/cv0.png');
					$('#'+app+' .fk').removeClass('fk');
				}
			}
		});
		return false;
	});

/***********************
 *  Actualiser
 ***********************/
	$('a.app_scan').click(function() {
		var app=$(this).parents('tr').attr('id');
		$.ajax({
			url: "scan.php",
            type: "GET",
            data: 'c=framapack&f',
		});
		$.ajax({
			url: "scan.php",
            type: "GET",
            data: 'c=framakey&f',
		});
		$.ajax({
			url: "scan.php",
            type: "GET",
            data: 'c='+app+'&f',
            success: function() {
				$('#'+app).removeAttr('id');window.location.hash = "#"+app;
				window.location.reload(); // Flemme de faire un rafraichissment dynamique
			}
		});
		return false;
	});
/***********************
 * Panier Framapack
 ***********************/
$('.table-sort td input:checkbox').on('click', function() {
    $('#cartcount').html($('.table-sort td input:checked').length);
    var link=''; var i=0;
    $('.table-sort td input:checked').each(function() {
        if (i>0) {link+='-';}
        link += $(this).val();
        i++;
    });
    $('#cartbox a').attr('href','http://www.framapack.org/?share='+link);
});
/************************
 * Description FS
 ************************/
var jsonFramasoft = [];
$.ajax({
	url: "cache/framasoft.txt",
    dataType: 'json',
    success: function(data) {
        jsonFramasoft = data;
    }
});

$('.description').on('mouseover', function() {
    var app = $(this).parents('tr').attr('id');
    $('#'+app+' .description span:eq(1)').html(jsonFramasoft[app]['desc']);
});

/*********************************
 * Afficher/Masquer les colonnes
 *********************************/
function colshow(id) {
    $('#bot_head th:eq('+id+')').show()
    $("#bot_results tr").each(function(index) {
        $(this).find('td:eq('+id+')').show();
    });
}
function colhide(id) {
    $('#bot_head th:eq('+id+')').hide()
    $("#bot_results tr").each(function(index) {
        $(this).find('td:eq('+id+')').hide();
    });
}
function colselect() {
    if($('#chk_fd').is(':checked')) {colshow(6);} else {colhide(6);}
    if($('#chk_fe').is(':checked')) {colshow(7);} else {colhide(7);}
    if($('#chk_fk').is(':checked')) {colshow(8);} else {colhide(8);}
    if($('#chk_fp').is(':checked')) {colshow(9);$('#cartbox').css('visibility','visible')} else {colhide(9);$('#cartbox').css('visibility','hidden')}
}
colselect();
$('#colselect input').on('click', function() {
    colselect()
});


